<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = array(
        ['name'=>'Banyo Dolapları'],
        ['name'=>'Duşakabinler'],
        ['name'=>'Duş Tekneleri'],
        ['name'=>'Küvetler'],
        ['name'=>'Aksesuarlar']
        
        );

        foreach($categories as $category) {
            $category_data = new Category();
            $category_data->name = $category['name'];
            $category_data->save();
        }

    }
}