<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBathroomCabinetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bathroom_cabinets', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('related_product_id')->default(0);
          $table->unsignedInteger('cover_id')->nullable();
          $table->unsignedInteger('sink_id')->nullable();
          $table->unsignedInteger('body_id')->nullable();
          $table->string('ust_dolap_yukseklik')->nullable();
          $table->string('ust_dolap_genislik')->nullable();
          $table->string('ust_dolap_derinlik')->nullable();
          $table->string('alt_dolap_yukseklik')->nullable();
          $table->string('alt_dolap_genislik')->nullable();
          $table->string('alt_dolap_derinlik')->nullable();
          $table->string('name');
          $table->text('details')->nullable();
          $table->string('product_image')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bathroom_cabinets');
    }
}
