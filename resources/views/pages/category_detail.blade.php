@extends('layouts.master')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col p-0">
      banner
  </div>
</div>

<div class="container mt-5">
  <div class="row mt-4">
    <div class="col-2">
      @foreach($category_menu as $category)
        @if(count($category->subcategories) == 0)
          <div>{{ $category->name }}</div>
        @else
          <div data-toggle="collapse" data-target="#{{str_slug($category->name)}}" aria-expanded="false" aria-controlls="{{str_slug($category->name)}}">{{ $category->name }}</div>
          <div class="collapse" id="{{str_slug($category->name)}}">
            @foreach($category->subcategories as $subcategory)
              <div>{{$subcategory->name}}</div>
            @endforeach
          </div>
        @endif
      @endforeach
    </div>
    <div class="col-10">

    </div>
  </div>
</div>



@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {

    $(".owl-carousel").owlCarousel({
      items: 5,
      margin:30,
      responsive: {
        0: {
          items: 2,
          margin:20
        },
        480: {
          items: 3
        },
        992: {
          items: 5,
          mouseDrag: false,
          touchDrag: false
        }
      }
    });


    var slider = new MasterSlider();
			slider.setup( 'masterslider', {
				width: 1410,
				height: 471,
				space: 5,
				minHeight: 250,
				layout: "fullwidth",
				autoplay: true,
				centerControls:false,
				loop: true,
				view:'parallaxMask'
					// more slider options goes here...
					// check slider options section in documentation for more options.
			} );
			// adds Arrows navigation control to the slider.
			slider.control( 'arrows' );
  })
</script>
@endsection
