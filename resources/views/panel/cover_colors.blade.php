@extends('layouts.panel_master')

@section('breadcrumb')


  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item active">Kapak Renkleri</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->


@endsection

@section('content')


  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 offset-md-10 text-right">
          <a href="{{ route('cover_colors.create') }}" class="btn btn-primary mb-3">Ekle</a>
        </div>
        <!-- /.col -->
        <div class="col-12">

          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="data_table" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Adı</th>
                      <th>Görsel</th>
                      <th style="width:120px; min-width:120px; max-width:120px;">İşlemler</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($cover_colors as $cover_color)
                    <tr>
                      <td>{{ $cover_color->name }}</td>
                      <td><img style="width:30px" src="{{ asset('images/products/'.$cover_color->color_image) }}"></td>
                      <td class="text-center">
                        <a class="btn btn-sm btn-info mr-2" href="{{ route('cover_colors.edit', ['id'=>$cover_color->id]) }}">Düzenle</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->



  <script>
     $(document).ready(function() {

    })
  </script>
@endsection
