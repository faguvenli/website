@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('cover_colors.index')}}">Kapak Renkleri</a></li>
            <li class="breadcrumb-item active">Kapak Rengi Düzenle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::model($cover_color,['route'=>['cover_colors.update',$cover_color->id],'method'=>'put', 'files'=>true, 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Renk Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          {{ Form::label('name', 'Adı', ['class' => 'col-form-label']) }}
                          <div class="input-group mb-3 col-12">
                            {{ Form::text('name', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                            <div class="input-group-append">
                                <span class="fas fa-vector-square fa-fw input-group-text"></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-12">{{ Form::label('color_image', 'Renk Görseli', ['class' => 'col-form-label']) }}</div>
                        <div class="col-12">
                          {{ Form::file('color_image', null, ['class'=>'form-control']) }}
                          <img style="width:30px;" src="{{ asset('images/products/'.$cover_color->color_image)}}">
                        </div>

                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
