@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('bathroom_cabinets.index')}}">Banyo Dolapları</a></li>
            <li class="breadcrumb-item active">Banyo Dolabı Ekle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::open(['route'=>'bathroom_cabinets.store', 'files'=>true, 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Ürün Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          <div class="col-12 col-md-4">
                            {{ Form::label('name', 'Adı', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('name', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('related_product_id', 'İlişikli Ürün', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('related_product_id',\App\BathroomCabinet::where('related_product_id',0)->get()->pluck('name','id'), null, ['class'=>'form-control selectpicker','placeholder'=>'Seçin','data-live-search'=>'true']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('cover_colors', 'Kapak Renkleri', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('cover_colors',\App\CoverColor::all()->pluck('name','id'), null, ['class'=>'form-control selectpicker', 'multiple'=>'', 'data-live-search'=>'true']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('cover_id', 'Kapak Tipi', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('cover_id',\App\Cover::all()->pluck('name','id'), null, ['class'=>'form-control selectpicker','data-live-search'=>'true']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('sink_id', 'Lavabo Tipi', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('sink_id',\App\Sink::all()->pluck('name','id'), null, ['class'=>'form-control selectpicker','data-live-search'=>'true']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('body_id', 'Gövde Tipi', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('body_id',\App\Body::all()->pluck('name','id'), null, ['class'=>'form-control selectpicker','data-live-search'=>'true']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('ust_dolap_yukseklik', 'Üst Dolap Yükseklik', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('ust_dolap_yukseklik',null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('ust_dolap_genislik', 'Üst Dolap Genişlik', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('ust_dolap_genislik',null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('ust_dolap_derinlik', 'Üst Dolap Derinlik', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('ust_dolap_derinlik',null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('alt_dolap_yukseklik', 'Alt Dolap Yükseklik', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('alt_dolap_yukseklik',null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('alt_dolap_genislik', 'Alt Dolap Genişlik', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('alt_dolap_genislik',null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                            {{ Form::label('alt_dolap_derinlik', 'Alt Dolap Derinlik', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('alt_dolap_derinlik',null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-12">{{ Form::label('product_image', 'Ürün Resmi', ['class' => 'col-form-label']) }}</div>
                          <div class="col-12">{{ Form::file('product_image', null, ['class'=>'form-control']) }}</div>
                          <div class="col-12">{{ Form::label('details', 'Detaylar', ['class' => 'col-form-label']) }}</div>
                          <div class="col-12">{{ Form::text('details', null, ['class'=>'form-control tinymce']) }}</div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection
