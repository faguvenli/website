<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PageController@getHome');
Route::get('/product/{id}/{name}', 'PageController@getProduct');

Route::get('/category/{id}/{name}', 'PageController@getCategory');

Route::middleware(['auth'])->group(function() {
  Route::resource('/panel/bathroom_cabinets','BathroomCabinetController');
  Route::resource('/panel/sinks','SinkController');
  Route::resource('/panel/covers','CoverController');
  Route::resource('/panel/bodies','BodyController');
  Route::resource('/panel/cover_colors','CoverColorController');
});
