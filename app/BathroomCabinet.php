<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BathroomCabinet extends Model
{
  protected $guarded = ['id'];
}
