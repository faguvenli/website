<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoverColor extends Model
{
    protected $guarded = ['id'];
}
