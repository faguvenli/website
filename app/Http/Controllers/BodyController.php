<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Body;
use Session;

class BodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $bodies = Body::all();
        return view('panel.bodies')->withBodies($bodies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.bodies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->only('name');


      //validate
      $this->validate($request, array(
        'name' => 'required',
      ));

      //store in db

      $body = Body::create($data);

      //redirect

      // Redirect
    Session::flash('success', 'Kayıt işlemi başarılı.');
    return redirect()->route('bodies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $body = Body::find($id);

      return view("panel.ops.bodies.update")
      ->with('body',$body);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name');

      $validator = $this->validate($request,array(
        'name' => "required",
      ));

      $body = Body::find($id);
      $body->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('bodies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
