<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\CoverColor;
use Session;
use Image;

class CoverColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $cover_colors = CoverColor::all();
        return view('panel.cover_colors')->with('cover_colors',$cover_colors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.cover_colors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->only('name');


      //validate
      $this->validate($request, array(
        'name' => 'required',
      ));

      if($request->color_image) {
        $request->validate(['color_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $color_image_name = str_slug($request->name).time().'.'.request()->color_image->getClientOriginalExtension();

        $location = public_path('images/products/'.$color_image_name);
        Image::make($request->file('color_image'))->resize(208,231)->save($location);
        $data['color_image'] = $color_image_name;
      }

      //store in db

      $cover_color = CoverColor::create($data);

      //redirect

      // Redirect
    Session::flash('success', 'Kayıt işlemi başarılı.');
    return redirect()->route('cover_colors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $cover_color = CoverColor::find($id);

      return view("panel.ops.cover_colors.update")
      ->with('cover_color',$cover_color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name');

      $validator = $this->validate($request,array(
        'name' => "required",
      ));

      if($request->color_image) {
        $request->validate(['color_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $old_photo = CoverColor::where('id',$id)->get();

        if($old_photo->first()->color_image) {
          //dd(asset('images/products/'.$old_photo->first()->product_image));
          File::delete(public_path('images/products/'.$old_photo->first()->color_image));
        }

        $color_image_name = str_slug($request->name).time().'.'.request()->color_image->getClientOriginalExtension();

        $location = public_path('images/products/'.$color_image_name);
        Image::make($request->file('color_image'))->resize(208,231)->save($location);
        $data['color_image'] = $color_image_name;
      }

      $cover_color = CoverColor::find($id);
      $cover_color->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('cover_colors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
