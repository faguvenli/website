<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class PageController extends Controller
{
    public function getHome() {
      $main_menu = Category::where('parent_id',0)->with('subcategories')->get();
        return view('pages.home')->with('main_menu',$main_menu);
    }

    public function getCategory($id) {
      $main_menu = Category::where('parent_id',0)->with('subcategories')->get();

      $category_menu = Category::where('parent_id', $id)->with('subcategories')->get();

        return view('pages.category_detail')
        ->with('main_menu',$main_menu)
        ->with('category_menu',$category_menu);
    }

}
