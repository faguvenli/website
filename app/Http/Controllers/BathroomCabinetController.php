<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\BathroomCabinet;
use Session;
use Storage;
use Image;

class BathroomCabinetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bathroom_cabinets = BathroomCabinet::all();
        return view('panel.bathroom_cabinets')->with('bathroom_cabinets',$bathroom_cabinets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.bathroom_cabinets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('name','details','related_product_id','cover_id','sink_id','body_id','ust_dolap_derinlik','ust_dolap_genislik','ust_dolap_yukseklik',
      'alt_dolap_derinlik','alt_dolap_genislik','alt_dolap_yukseklik');

      //validate
      $this->validate($request, array(
        'name' => 'required'
      ));

      if($request->related_product_id != null) {
        $data['related_product_id'] = $request->input('related_product_id');
      } else {
        $data['related_product_id'] = 0;
      }

      if($request->product_image) {
        $request->validate(['product_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $product_image_name = str_slug($request->name).time().'.'.request()->product_image->getClientOriginalExtension();

        $location = public_path('images/products/'.$product_image_name);
        Image::make($request->file('product_image'))->resize(208,231)->save($location);
        $data['product_image'] = $product_image_name;
      }

      //store in db

      $bathroom_cabinet = BathroomCabinet::create($data);

      //redirect

      // Redirect
    Session::flash('success', 'Kayıt işlemi başarılı.');
    return redirect()->route('bathroom_cabinets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bathroom_cabinet = BathroomCabinet::find($id);

      return view("panel.ops.bathroom_cabinets.update")
      ->with('bathroom_cabinet',$bathroom_cabinet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name','details','related_product_id','cover_id','sink_id','body_id','ust_dolap_derinlik','ust_dolap_genislik','ust_dolap_yukseklik',
    'alt_dolap_derinlik','alt_dolap_genislik','alt_dolap_yukseklik');

      $validator = $this->validate($request,array(
        'name' => "required",
      ));

      if($request->related_product_id != null) {
        $data['related_product_id'] = $request->input('related_product_id');
      } else {
        $data['related_product_id'] = 0;
      }

      if($request->product_image) {
        $request->validate(['product_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $old_photo = BathroomCabinet::where('id',$id)->get();

        if($old_photo->first()->product_image) {
          //dd(asset('images/products/'.$old_photo->first()->product_image));
          File::delete(public_path('images/products/'.$old_photo->first()->product_image));
        }

        $product_image_name = str_slug($request->name).time().'.'.request()->product_image->getClientOriginalExtension();

        $location = public_path('images/products/'.$product_image_name);
        Image::make($request->file('product_image'))->resize(208,231)->save($location);
        $data['product_image'] = $product_image_name;
      }

      $bathroom_cabinet = BathroomCabinet::find($id);
      $product->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('bathroom_cabinets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
