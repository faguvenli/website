<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cover;
use Session;

class CoverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $covers = Cover::all();
        return view('panel.covers')->withCovers($covers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.covers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->only('name');


      //validate
      $this->validate($request, array(
        'name' => 'required',
      ));

      //store in db

      $cover = Cover::create($data);

      //redirect

      // Redirect
    Session::flash('success', 'Kayıt işlemi başarılı.');
    return redirect()->route('covers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $cover = Cover::find($id);

      return view("panel.ops.covers.update")
      ->with('cover',$cover);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name');

      $validator = $this->validate($request,array(
        'name' => "required",
      ));

      $cover = Cover::find($id);
      $cover->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('covers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
