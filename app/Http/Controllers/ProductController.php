<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Product;
use Session;
use Storage;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = Product::all();
        return view('panel.products')->withProducts($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.ops.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->only('name','category_id','details','related_product_id');

      //validate
      $this->validate($request, array(
        'name' => 'required',
        'category_id' => 'required'
      ));

      if($request->related_product_id != null) {
        $data['related_product_id'] = $request->input('related_product_id');
      } else {
        $data['related_product_id'] = 0;
      }

      if($request->product_image) {
        $request->validate(['product_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $product_image_name = str_slug($request->name).time().'.'.request()->product_image->getClientOriginalExtension();

        $location = public_path('images/products/'.$product_image_name);
        Image::make($request->file('product_image'))->resize(208,231)->save($location);
        $data['product_image'] = $product_image_name;
      }

      //store in db

      $product = Product::create($data);

      //redirect

      // Redirect
    Session::flash('success', 'Kayıt işlemi başarılı.');
    return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $product = Product::find($id);

      return view("panel.ops.products.update")
      ->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('name','category_id','details','related_product_id');

      $validator = $this->validate($request,array(
        'name' => "required",
        'category_id' => "required"
      ));

      if($request->related_product_id != null) {
        $data['related_product_id'] = $request->input('related_product_id');
      } else {
        $data['related_product_id'] = 0;
      }

      if($request->product_image) {
        $request->validate(['product_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $old_photo = Product::where('id',$id)->get();

        if($old_photo->first()->product_image) {
          //dd(asset('images/products/'.$old_photo->first()->product_image));
          File::delete(public_path('images/products/'.$old_photo->first()->product_image));
        }

        $product_image_name = str_slug($request->name).time().'.'.request()->product_image->getClientOriginalExtension();

        $location = public_path('images/products/'.$product_image_name);
        Image::make($request->file('product_image'))->resize(208,231)->save($location);
        $data['product_image'] = $product_image_name;
      }

      $product = Product::find($id);
      $product->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');
      return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
